(function(angular) {
    'use strict';

    angular
        .module('ui.breadcrumb')
        .directive('uiBreadcrumb', uiBreadcrumb);

    uiBreadcrumb.$inject = ['$breadcrumb'];

    function uiBreadcrumb($breadcrumb) {
        function BreadcrumbController($rootScope, $breadcrumb) {
            var vm = this;

            $rootScope.$on('$viewContentLoaded', updateSteps);
            updateSteps();

            function updateSteps() {
                $breadcrumb.getSteps().then(function (steps) {
                    vm.steps = steps;
                });
            }
        }

        BreadcrumbController.$inject = ['$rootScope', '$breadcrumb'];

        return {
            restrict: 'AE',
            templateUrl: $breadcrumb.getOption('templateUrl'),
            controller: BreadcrumbController,
            controllerAs: 'vm',
            bindToController: true
        };
    }

})(angular);
